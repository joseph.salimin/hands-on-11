# NOT USED

import glob
import json

unused_data = [
	'id',
	'spell',
	'status',
	'favorites',
	'location_source',
	'has_phone_param',
	'republish_date',
	'created_at',
	'partner_id',
	'category_id',
	'valid_to',
	'partner_code',
	'views',
	'monetizationInfo',
	'package',	
	'revision',
	'replies',
	'display_date',	
	'user_id',
	'calls',
	'created_at_first',
	'locations',
	'images',
]

def clean_data(data):
    for i in range(len(data)):
        for unused in unused_data:
            if unused in data[i]:
                del data[i][unused]
    return data


files = glob.glob("data/*")
list_data = []

for file in files:
	data = None
	with open(file) as f:
		s = f.read()
		data = json.loads(s)

	if data is not None:
		with open('data_cleaned/{}'.format(file.split("/")[1]), 'w') as f:
			f.write(json.dumps(clean_data(data)))