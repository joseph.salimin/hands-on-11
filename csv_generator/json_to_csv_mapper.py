import glob
import json
import pandas as pd


unused_data = [
	'id',
	'spell',
	'status',
	'favorites',
	'location_source',
	'has_phone_param',
	'republish_date',
	'created_at',
	'partner_id',
	'category_id',
	'valid_to',
	'partner_code',
	'views',
	'monetizationInfo',
	'package',	
	'revision',
	'replies',
	'display_date',	
	'user_id',
	'calls',
	'created_at_first',
	'locations',
	'images',
]

def clean_data(data):
    for i in range(len(data)):
        for unused in unused_data:
            if unused in data[i]:
                del data[i][unused]
    return data


def remove_unused_data_from_files(files):
	file_data_list = []

	for file in files:
		data = None
		with open(file) as f:
			s = f.read()
			data = json.loads(s)

		if data is not None:
			data = clean_data(data)
			file_data_list += data

	return file_data_list


def find_index_from_parameters(params, key):
	for i, val in enumerate(params):
		if val["key"] == key:
			return i

	return -1


def append_parameters(new_row, row):
	keys = [
		"make",
		"m_tipe",
		"m_tipe_variant",
		"m_year",
		"mileage",
		"m_fuel",
		"m_color",
		"m_transmission",
		"m_body",
		"m_engine_capacity",
		"m_feature",
		"m_seller_type",
	]

	parameters = row["parameters"]
	for key in keys:
		idx = find_index_from_parameters(parameters, key)

		if idx == -1:
			new_row.append(None)
		else:
			data = parameters[idx]

			if data["type"] == 'single':
				new_row.append(data["value"])
			else:
				new_row.append(",".join([val["value"] for val in data["values"]]))


def append_location(new_row, row):
	location = row["locations_resolved"]

	new_row += [
		location.get("COUNTRY_name", None),
		location.get("COUNTRY_id", None),
		location.get("ADMIN_LEVEL_1_name", None),
		location.get("ADMIN_LEVEL_1_id", None),
		location.get("ADMIN_LEVEL_3_name", None),
		location.get("ADMIN_LEVEL_3_id", None),
		location.get("SUBLOCALITY_LEVEL_1_name", None),
		location.get("SUBLOCALITY_LEVEL_1_id", None),
	]


def generate_header():
	return [
		"make",
		"m_type",
		"m_type_variant",
		"m_year",
		"mileage",
		"m_fuel",
		"m_color",
		"m_transmission",
		"m_body",
		"m_engine_capacity",
		"m_feature",
		"m_seller_type",
		"country",
		"country_id",
		"region",
		"region_id",
		"city",
		"city_id",
		"district",
		"district_id",
		"score",
		"price",
	]


def generate_body_from_files(file_data_list):
	data_list = []

	for row in file_data_list:		
		new_row = []
		append_parameters(new_row, row)
		append_location(new_row, row)
		new_row += [row["score"], row["price"]["value"]["raw"]]

		data_list.append(new_row)

	return data_list


# Clean data
files = glob.glob("data/*")
file_data_list = remove_unused_data_from_files(files)

# Generate data_list
data_list = [generate_header()]
data_list += generate_body_from_files(file_data_list)

df = pd.DataFrame(data_list[1:], columns=data_list[0])
print(df.head())
print(df["m_feature"])

df.to_csv('dataset.csv', index=False)